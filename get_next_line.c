/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: csouza-f <csouza-f@student.42sp.org.br>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/16 17:46:23 by csouza-f          #+#    #+#             */
/*   Updated: 2020/03/18 16:22:53 by csouza-f         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"

int			get_next_line(int fd, char **line)
{
    int				ret;
	int				pos;
	char			*aux;
    char			*buffer;
	static char		*tmp;

	(!tmp) ? tmp = ft_strdup("") : 0;
	if (!(aux = ft_strdup("")) && !line && BUFFER_SIZE <= 0 && fd == -1)
		return (-1);
	if (!(buffer = malloc((BUFFER_SIZE + 1) * sizeof(char))))
		return (-1);
    while ((ret = read(fd, buffer, BUFFER_SIZE)))
	{
		buffer[ret] = '\0';
		aux = ft_strjoin(aux, buffer);
		if ((ft_strchr_pos(aux, '\n') >= 0))
			break ;
	}
	aux = ft_strjoin(tmp, aux);
	free(tmp);
	pos = ft_strchr_pos(aux, '\n');
	if (pos >= 0)
	{
		*line = ft_substr(aux, 0, pos);
		tmp = ft_strdup(ft_substr(aux, pos + 1, ft_strlen(aux)));
		return (1);
	}
	else
	{
		*line = ft_substr(aux, 0, ft_strlen(aux));
		return (0);
	}
	return (-1);
}
